﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombScript : MonoBehaviour {

	float timer = 1.5f;
	Collider[] hitColliders;
	public float blastRadius= 20;
	//public AudioClip explosionClip;
	AudioSource bombAudio;


	// Use this for initialization
	void Awake () {
		bombAudio = GetComponent <AudioSource> ();
		
	}

	void Explode(Vector3 explosionPoint){

		hitColliders = Physics.OverlapSphere (transform.position, blastRadius);

		foreach (Collider hitCol in hitColliders) {
			print(hitCol.gameObject.name);
			if (hitCol.gameObject.tag == "Enemy") {
				EnemyHealth enemyHealth = hitCol.gameObject.GetComponent <EnemyHealth> ();
				if(enemyHealth != null)
				{
					enemyHealth.TakeDamage (50,transform.position);

				}
			}
		}

	}


	// Update is called once per frame
	void Update () {
		if (timer > 0) {
			timer -= Time.deltaTime;

		}

		if (timer <= .5) {
			this.GetComponent<Light> ().intensity = 100;
			this.GetComponent<Light> ().range = 5;
			bombAudio.Play ();
		}

		if (timer <= 0) {

			Explode (transform.position);
			bombAudio.Play ();
			Destroy (gameObject);

		}


	}
}
