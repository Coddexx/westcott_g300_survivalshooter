﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class KeyScript : MonoBehaviour {

	public Text keyImage;
	public Color keyColor = new Color(1f, 1f, 1f, 1f);

	MeshRenderer keyMesh;
	Light keyLight;

	public bool hasKey = false; 

	// Use this for initialization
	void Start () {
		keyMesh = GetComponent<MeshRenderer> ();
		keyLight = GetComponent<Light> ();
	}

	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.name == "Player")
		{
			keyMesh.enabled = false;
			keyLight.enabled = false;

			//Destroy (gameObject.);
			keyImage.color= keyColor ;
			print ("collided with key");
			hasKey = true;
			//print (hasKey);
		}
	}

}
