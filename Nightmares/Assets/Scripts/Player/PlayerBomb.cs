﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerBomb : MonoBehaviour {

	public GameObject bomb;
	Vector3 offset = new Vector3 (0,1,0);
	int speed = 5;
	float moveHorizontal;
	float moveVertical;
	float bombTimer = 10;
	public Slider bombSlider;

	// Use this for initialization
	void Awake () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 forwardVel = transform.forward * speed;
		
		if (bombTimer >= 0) {
			bombTimer += Time.deltaTime;
			bombSlider.value = bombTimer/10;
		}
		if (bombTimer >= 10) {
			
			if(Input.GetKeyDown("space") )
			{
				print ("button pressed");
				GameObject freshBomb = Instantiate (bomb, transform.position+ offset, Quaternion.identity);
				Rigidbody rb = freshBomb.GetComponent<Rigidbody> ();
				rb.velocity = forwardVel;
				bombTimer = 0;
				bombSlider.value = bombTimer;
			}
		}


	
	}
}
