﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public GameObject enemy;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;

	float playerDistance;
	public int spawnRange= 40;
	GameObject Player;

    void Start ()
    { 
		Player= GameObject.FindWithTag ("Player");

		InvokeRepeating ("Spawn", spawnTime, spawnTime);
    }

	void Update(){
		//playerDistance = ((transform.position + transform.position.magnitude) + (Player.transform.position + Player.transform.position.magnitude)) / 2f;
		playerDistance = Vector3.Distance(transform.position, Player.transform.position);
		//print (playerDistance);
	}

   void Spawn ()
    {
        if(playerHealth.currentHealth <= 0f)
        {
            return;
        }

		else if(playerDistance>= spawnRange){
			return;
		}
       
		int spawnPointIndex = Random.Range (0, spawnPoints.Length);


			Instantiate (enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);

    }
}
