﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryTextManager : MonoBehaviour {
	public Text victoryText;
	public bool hasWon = false;
	public Color victoryColor = new Color(202f, 255f, 15f, 1f);
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (hasWon == true) {
			victoryText.color = victoryColor;
		}
	}
}
