﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TutorialText : MonoBehaviour {
	float timer = 10f;
	public Color newColor = new Color(1f, 1f, 1f, 0f);
	public Text tutorialText;
	// Use this for initialization
	void Start () {
		
	}
	
	void Update () {
		if (timer > 0) {
			timer -= Time.deltaTime;

		}


		if (timer <= 0) {

			tutorialText.color = newColor; 

		}

}
}
