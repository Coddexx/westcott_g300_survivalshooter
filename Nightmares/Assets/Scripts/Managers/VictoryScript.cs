﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryScript : MonoBehaviour {
	public GameObject textObject;
	VictoryTextManager victoryTextManager;
	void Awake ()
	{
		
		victoryTextManager = textObject.GetComponent<VictoryTextManager> ();

	}



	void OnTriggerEnter(Collider other){
		if (other.tag == "Player") {
			print ("victory");
			victoryTextManager.hasWon = true;
		}
	}

	
	// Update is called once per frame
	void Update () {
		
	}
}
